﻿using ado.net_lesson3.DataAccess;
using ado.net_lesson3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Repository repository = new Repository())
            {
                Person person = new Person()
                {
                    LastName = "Tyo",
                    FirstName = "Ruslan",
                    MiddleName = "Sergeevich",
                    GenderId = 1
                };
                //repository.SavePerson(person);

                var result = repository.GetPersonById(1);
                Console.WriteLine($"ID:{result.ID}\nLastName:{result.LastName}\nFirstName:{result.FirstName}");
                Console.ReadLine();
            }

        }
    }
}
