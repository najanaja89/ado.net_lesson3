﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson3.DataAccess
{
    public class BaseRepository : IDisposable
    {
    
        public SqlConnection _connection{ get; set; }

        public BaseRepository()
        {
            //_connection = new SqlConnection(ConfigurationManager.AppSettings["JournalMarkDB"].);
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["JournalMarkDB"].ConnectionString);
            _connection.Open();
        }

        public void Dispose()
        {
            _connection.Close();
        }
    }
}
