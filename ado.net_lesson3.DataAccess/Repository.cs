﻿using ado.net_lesson3.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson3.DataAccess
{
    public class Repository : BaseRepository
    {
        public void SavePerson(Person person)
        {
            string query = @"INSERT INTO Person (LastName, FirstName, MiddleName, GenderId) 
VALUES(@lastName, @firstName, @midlName, @genderId)";

            SqlCommand command = new SqlCommand(query, _connection);
            //SqlParameter par = new SqlParameter();
            command.Parameters.AddRange(new SqlParameter[]
            {
                new SqlParameter("@lastName", SqlDbType.NVarChar) { Value = person.LastName},
                new SqlParameter("@firstName", SqlDbType.NVarChar) { Value = person.FirstName},
                new SqlParameter("@midlName", SqlDbType.NVarChar) { Value = person.MiddleName},
                new SqlParameter("@genderId", SqlDbType.Int) { Value = person.GenderId}
            });
            command.ExecuteNonQuery();
            command.Dispose();
        }

        public Person GetPersonById(int id)
        {
            Person person = new Person();
            string query = "select * from person where id = @id";
            using (SqlCommand command = new SqlCommand(query, _connection))
            {
                command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Value = id });
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    reader.Read();
                    person.ID = (int)reader["Id"];
                    person.LastName = reader["LastName"].ToString();
                    person.FirstName = reader["FirstName"].ToString();
                    person.MiddleName = reader["MiddleName"].ToString();
                    person.GenderId = (int)reader["GenderId"];
                }
            }
            return person;
        }
    }
 
}
